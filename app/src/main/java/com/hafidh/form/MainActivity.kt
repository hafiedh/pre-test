package com.hafidh.form

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.hafidh.form.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    // view binding
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // init view binding
        binding = ActivityMainBinding.inflate(layoutInflater)
        // set content with view binding
        setContentView(binding.root)
        // call method set Image
        setImage()
        // call method set Spinner
        initSpinner()
    }

    // init spinner province
    private fun initSpinner() {

        // adapter
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.province,
            android.R.layout.simple_spinner_item
        )
        // set dropdown view
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // set adapter
        binding.spProvinsi.adapter = adapter
    }

   private fun setImage(){
       // set image
       binding.imgLogo.setImageResource(R.drawable.bug)
   }

}